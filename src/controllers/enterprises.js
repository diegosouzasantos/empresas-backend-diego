const { enterprisesService } = require('../services');

module.exports = {
    async create(req, res) {
        try {
            const enterprise = req.body;
            await enterprisesService.create(enterprise);
            res.end();
        } catch (error) {
            console.error(error);
            res.status(error.status).json(error.message);
        }
    },
    async get(req, res) {
        try {
            const { id } = req.params;
            const response = await enterprisesService.get(id);

            Object.keys(response.headers).forEach((key) => {
                res.setHeader(key, response.headers[key]);
            });

            res.json(response.body);
        } catch (error) {
            console.error(error);
            res.status(error.status).json(error.message);
        }
    },
    async update(req, res) {
        try {
            const enterprise = req.body;
            const { id } = req.params;
            await enterprisesService.update(id, enterprise);
            res.end();
        } catch (error) {
            console.error(error);
            res.status(error.status).json(error.message);
        }
    },
    async delete(req, res) {
        try {
            const { id } = req.params;
            await enterprisesService.delete(id);
            res.end();
        } catch (error) {
            console.error(error);
            res.status(error.status).json(error.message);
        }
    },
};
const router = require('express').Router();
const { usersController} = require('../controllers');
//const { authMiddleware } = require('../middlewares');

router.post('/users/', /*authMiddleware,*/ usersController.create);

router.get('/users/:_id', /*authMiddleware,*/ usersController.get);

router.patch('/users/:_id', /*authMiddleware,*/ usersController.update);

router.delete('/users/:_id', /*authMiddleware,*/ usersController.destroy);

module.exports = router;
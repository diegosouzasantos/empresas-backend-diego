const router = require('express').Router();
const { enterprisesController} = require('../controllers');

router.post('/enterprises/', enterprisesController.create);

router.get('/enterprises/', enterprisesController.list);

router.get('/enterprises/:_id', enterprisesController.get);
router.get('/enterprises/:name', enterprisesController.getByName);
router.get('/enterprises/:type', enterprisesController.getByType);

router.patch('/enterprises/:_id', enterprisesController.update);

router.delete('/enterprises/:_id', enterprisesController.destroy);

module.exports = router;